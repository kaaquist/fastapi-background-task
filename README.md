## Fastapi with background task
> fastapi testing background task

First install needed libs:
```
> pip install -r requirements.txt
```
Start fastapi app:
```
> uvicorn main:app --reload 
```
Open a new window in your terminal and run test:
```
> python tests/background_test.py
```
