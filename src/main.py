from time import sleep
from fastapi import FastAPI, BackgroundTasks, Request
from fastapi.encoders import jsonable_encoder
from pydantic import BaseModel
from datetime import datetime
import json

app = FastAPI()


class UserInfo(BaseModel):
    name: str
    address: str
    

def delete_objects(filename: str):
    print(f"Deleting: {filename}")
    for i in range(0, 2):
        sleep(2)
        print(f"#{i} File deleted: {filename}")

@app.get("/")
async def root(background_tasks: BackgroundTasks):
    background_tasks.add_task(delete_objects, f"my filename {datetime.now()}")
    return {"hello": "world"}


@app.post("/json")
async def good_json(data: Request):
    the_json = jsonable_encoder(data.data)
    print(f"The name: {the_json}")
    return {"status": "OK"}
