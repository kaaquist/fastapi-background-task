import httpx

# Response is returned and the background task will execute
r = httpx.get("http://localhost:8000")

# Response not returned due to client termination and the background task will not execute
r = httpx.get("http://localhost:8000")

r = httpx.post("http://localhost:8000/json", json={'name': 'Kasper', 'address': 'my address'})